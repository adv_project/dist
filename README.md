# ADV Packaging Tool

## Archlinux

To build the ADV package for Archlinux, enter the `archlinux` directory
and run `makepkg` as normal user:

```
cd archlinux/
makepkg
```

If you want the last (unstable) version, run `makepkg devel=true` instead.

To install the package on your system, run as root user:

```
pacman -U adv-VERSION-ARCH.pkg.tar.xz
```

